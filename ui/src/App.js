import React, { Component } from 'react';
import Header from './Header/Header';
import Table from './Table/Table';
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Table />
      </div>
    );
  }
}

export default App;
