export default function userReducer(
  state = {
    names: ["Test"],
    name: ""
  },
  action
) {
  switch (action.type) {
    case "SELECT_USER":
      return { ...state, name: "Pedro" };
    case "ADD_USER":
      return { ...state, names: [...state.names, action.payload.name] };
    case "UPDATE_USER_NAME":
      return { ...state, name: action.payload.name };
    case "UPDATE_USER_NAMES":
      return { ...state, names: [...state.names, ...action.payload.names] };
    case "DELETE_USER_NAME":
      return {
        ...state,
        names: state.names.filter((f, i) => i !== action.payload.index)
      };
    default:
      return state;
  }
}
