import userReducer from './userReducer';

describe('userReducer', () => {
  it('should check initial state', () => {
    let state = userReducer(undefined, { type: '', payload: undefined });
    let expectedState = { names: ['Test'], name: '' };
    expect(state).toEqual(expectedState);
  });

  it('should check SELECT_USER state', () => {
    let state = userReducer(undefined, { type: 'SELECT_USER', payload: {} });
    let expectedState = { names: ['Test'], name: 'Pedro' };
    expect(state).toEqual(expectedState);
  });

  it('should check ADD_USER state', () => {
    let state = userReducer(undefined, { type: 'ADD_USER', payload: {name: 'Lucas'} });
    let expectedState = { names: ['Test', 'Lucas'], name: '' };
    expect(state).toEqual(expectedState);
  });

  it('should check UPDATE_USER_NAME state', () => {
    let state = userReducer(undefined, { type: 'UPDATE_USER_NAME', payload: {name: 'Lucas'} });
    let expectedState = { names: ['Test'], name: 'Lucas' };
    expect(state).toEqual(expectedState);
  });
  
  it('should check UPDATE_USER_NAMES state', () => {
    let state = userReducer(undefined, { type: 'UPDATE_USER_NAMES', payload: {names: ['Test1', 'Test2']} });
    let expectedState = { names: ['Test', 'Test1', 'Test2'], name: '' };
    expect(state).toEqual(expectedState);
  });


  it('should check DELETE_USER_NAME state', () => {
    let state = userReducer(undefined, { type: 'DELETE_USER_NAME', payload: {index: 0} });
    let expectedState = { names: [], name: '' };
    expect(state).toEqual(expectedState);
  });

});
