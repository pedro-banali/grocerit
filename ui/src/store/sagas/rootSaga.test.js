import rootSaga from './rootSaga';
import { all } from 'redux-saga/effects';
import { watchUsersAsync } from './userSaga';

describe('Root Saga', () => {
    it('should check if contains array of sagas', () => {
        const allSaga = rootSaga();
        expect(allSaga.next().value).toEqual(all([watchUsersAsync()]));
    });
});