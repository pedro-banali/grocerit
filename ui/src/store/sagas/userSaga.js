import { fetch } from "../api";
import { put, takeEvery, call } from "redux-saga/effects";

export function* getUsersAsync() {
  const users = yield call(fetch, `users`);

  yield put({
    type: "UPDATE_USER_NAMES",
    payload: {
      names: users.data.reduce((curr, next) => [...curr, next.name], [])
    }
  });
}

export function* watchUsersAsync() {
  yield takeEvery("SELECT_USER_ASYNC", getUsersAsync);
}
