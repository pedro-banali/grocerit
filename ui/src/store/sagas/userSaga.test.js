import { getUsersAsync } from "./userSaga";
import { fetch } from "../api";
import { put, call } from "redux-saga/effects";
describe("User Saga", () => {
  it("get users saga test", () => {
    const users = {
      data: [
        {
          address: {
            street: "Kulas Light",
            suite: "Apt. 556",
            city: "Gwenborough",
            zipcode: "92998-3874"
          },
          company: {
            name: "Romaguera-Crona",
            catchPhrase: "Multi-layered client-server neural-net",
            bs: "harness real-time e-markets"
          },
          email: "Sincere@april.biz",
          id: 1,
          name: "Leanne Graham",
          phone: "1-770-736-8031 x56442",
          username: "Bret",
          website: "hildegard.org"
        },
        {
          address: {
            street: "Victor Plains",
            suite: "Suite 879",
            city: "Wisokyburgh",
            zipcode: "90566-7771"
          },
          company: {
            name: "Deckow-Crist",
            catchPhrase: "Proactive didactic contingency",
            bs: "synergize scalable supply-chains"
          },
          email: "Shanna@melissa.tv",
          id: 2,
          name: "Ervin Howell",
          phone: "010-692-6593 x09125",
          username: "Antonette",
          website: "anastasia.net"
        }
      ]
    };

    const expectedUsers = { names: ["Leanne Graham", "Ervin Howell"] };

    const userSaga = getUsersAsync();

    expect(userSaga.next().value).toEqual(call(fetch, "users"));
    expect(userSaga.next(users).value).toEqual(
      put({
        type: "UPDATE_USER_NAMES",
        payload: expectedUsers
      })
    );
    expect(userSaga.next()).toEqual({ done: true, value: undefined });
  });
});
