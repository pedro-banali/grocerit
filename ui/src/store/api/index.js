import axios from "axios";

const endPoint = `https://jsonplaceholder.typicode.com/`;

export const fetch = (url, params) =>
  axios.get(`${endPoint}${url}`, { ...params });

export const post = (url, params) =>
  axios.post(`${endPoint}${url}`, { ...params });
