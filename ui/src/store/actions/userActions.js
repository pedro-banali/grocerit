export function fetchUser() {
  return { type: "SELECT_USER" };
}

export function fetchUserAsync() {
  return { type: "SELECT_USER_ASYNC" };
}

export function addUserName(name) {
  return {
    type: "ADD_USER",
    payload: { name }
  };
}

export function updateUserName(name) {
  return {
    type: "UPDATE_USER_NAME",
    payload: { name }
  };
}

export function deleteUserName(index) {
  return {
    type: "DELETE_USER_NAME",
    payload: { index }
  };
}