import {fetchUser, fetchUserAsync, addUserName, updateUserName, deleteUserName } from './userActions';

describe('User Actions', () => {

    it('should create fetch user action', () => {
        let fetchUserAction = fetchUser();
        expect(fetchUserAction).toEqual({ type: "SELECT_USER" });
    });

    it('should create fetch user async actions', () => {
        let fetchUserAction = fetchUserAsync();
        expect(fetchUserAction).toEqual({ type: "SELECT_USER_ASYNC" });
    });

    it('should create update user name action', () => {
        let name = 'Test2';
        let addUserNameAction = addUserName(name);
        let expectedAction = {
            type: "ADD_USER",
            payload: { name }
          };
        expect(addUserNameAction).toEqual(expectedAction);
    });

    it('should create update user name action', () => {
        let name = 'Test';
        let updateUserNameAction = updateUserName(name);
        let expectedAction = {
            type: "UPDATE_USER_NAME",
            payload: { name }
          };
        expect(updateUserNameAction).toEqual(expectedAction);
    });

    it('should create delete user name action', () => {
        let index = 0;
        let deleteUserNameAction = deleteUserName(index);
        let expectedAction = {
            type: "DELETE_USER_NAME",
            payload: { index }
          };
        expect(deleteUserNameAction).toEqual(expectedAction);
    });
});