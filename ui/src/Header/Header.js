import React from 'react';
import './Header.scss';
const Header = (props) => {
    return (
        <header className="header">
            <div className="title">Contact List</div>
        </header>
    );
}

export default Header;