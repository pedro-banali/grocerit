//@flow
import React from "react";
import { connect } from "react-redux";
import {
  fetchUser,
  fetchUserAsync,
  addUserName,
  updateUserName,
  deleteUserName
} from "../store/actions/userActions";

import "./Table.scss";

export const Table = props => {
  return (
    <div className="table">
      <input
        className="input"
        placeholder="Enter a contact name ..."
        type="text"
        value={props.name}
        onChange={e => props.updateUserName(e.target.value)}
      />

      <button
        className="add"
        type="button"
        onClick={e => {
          e.preventDefault();
          props.addUserName(props.name);
          props.updateUserName("");
        }}
      >
        Add
      </button>

      <button
        className="add"
        type="button"
        onClick={e => {
          e.preventDefault();
          props.fetchUserAsync("");
        }}
      >
        Fetch
      </button>
      <div>{props.name}</div>
      <div>
        <ul>
          {props.names.map((name, i) => (
            <li key={i}>
              {name}{" "}
              <button
                className="delete"
                onClick={() => {
                  props.deleteUserName(i);
                }}
              >
                {" "}
                X{" "}
              </button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    name: state.userReducer.name,
    names: state.userReducer.names
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchUser: () => {
      dispatch(fetchUser());
    },
    fetchUserAsync: () => {
      dispatch(fetchUserAsync());
    },
    addUserName: name => {
      dispatch(addUserName(name));
    },
    updateUserName: name => {
      dispatch(updateUserName(name));
    },
    deleteUserName: name => {
      dispatch(deleteUserName(name));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Table);
