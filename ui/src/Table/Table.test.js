import { Table } from "./Table";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
describe("Table", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Table names={["test"]} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});
