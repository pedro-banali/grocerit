const express = require('express');
const router = express.Router();
const UserController = require('../User/UserController');

class Routes {

  constructor() {
    this.router = router;
    this.userController =new UserController();
    this.config();
  }

  config() {
    this.router.get("/", (req, res) => res.status(200).send("asdas"));
    this.router.get("/user", (req, res) => this.userController.root(req, res));
  }
}

module.exports =  Routes;