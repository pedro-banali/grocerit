const App = require('./app');


const PORT = 3000;

const appStart = new App();

appStart.app.listen(PORT, () => {
  console.log(`Express server listening on port ${PORT}`);
});
