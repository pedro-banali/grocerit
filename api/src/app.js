const express = require('express');
const bodyParser = require('body-parser');
const Routes = require('./routes/Routes');

class App{
    constructor() {
        this.app = express();
        this.routes = new Routes();
        this.config();
    }

    config() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));

        this.app.use('/', this.routes.router);
    }
}

module.exports = App;